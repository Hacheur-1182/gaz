import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gaz/provider/track_provider.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import '../../helpers/helpers.dart';
import '../../provider/authentication_provider.dart';

import '../../routes/app_route.dart';
import '../bottom_sheet_modal.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool _isLoading = false;
  @override
  void initState() {
    super.initState();
    // context.read<TrackGazProvider>().getParameters();
    getData();
  }

  Future<void> getData() async {
    try {
      InternetAddress.lookup("google.com").then((result) {
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          context.read<TrackGazProvider>().getParameters().then((_) => {
                if (mounted)
                  {
                    setState(() {
                      _isLoading = false;
                    })
                  }
              });
        }
      }).catchError((onError) {
        Navigator.pushNamed(context, AppRoutes.connectionErrorPage);
        Fluttertoast.showToast(
          msg: "Sorry, No internet connection",
          backgroundColor: Colors.redAccent,
          textColor: Colors.white,
        );
      });
    } on SocketException catch (_) {
      Navigator.pushNamed(context, AppRoutes.connectionErrorPage);
      Fluttertoast.showToast(
        msg: "Sorry, No internet connection",
        backgroundColor: Colors.redAccent,
        textColor: Colors.white,
      );
    }

    setState(() {
      _isLoading = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    final params =
        Provider.of<TrackGazProvider>(context, listen: false).parameters;
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(
          top: 70.0,
          left: 20.0,
          right: 20.0,
        ),
        child: _isLoading
            ? const Center(
                child: CircularProgressIndicator(
                  color: primaryColor,
                ),
              )
            : Column(
                children: [
                  Row(
                    children: [
                      const Text(
                        "Dashboard",
                        style: TextStyle(
                          fontSize: 25.0,
                        ),
                      ),
                      const Spacer(),
                      IconButton(
                        onPressed: () {
                          Provider.of<AuthenticationProvider>(context,
                                  listen: false)
                              .logout();
                        },
                        icon: SvgPicture.asset("assets/icon/logout.svg"),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.12,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(
                              width: 1,
                              color: Colors.grey.shade300,
                            ),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 30.0),
                                child: Row(
                                  children: [
                                    const Text(
                                      "Refresh",
                                      style: TextStyle(
                                        fontSize: 18.0,
                                      ),
                                    ),
                                    const Spacer(),
                                    IconButton(
                                      onPressed: () {
                                        Navigator.pushReplacementNamed(
                                          context,
                                          AppRoutes.homePage,
                                        );
                                      },
                                      icon: const Icon(
                                        CupertinoIcons.refresh,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Expanded(
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.all(20.0),
                            height: MediaQuery.of(context).size.height * 0.7,
                            width: double.infinity,
                            clipBehavior: Clip.antiAlias,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(
                                width: 1.0,
                                color: Colors.grey.shade300,
                              ),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    const Text(
                                      "Gaz",
                                      style: TextStyle(
                                        fontSize: 25.0,
                                      ),
                                    ),
                                    const Spacer(),
                                    TextButton(
                                      style: TextButton.styleFrom(
                                        padding: const EdgeInsets.all(2),
                                      ),
                                      onPressed: () {
                                        showModalBottomSheet(
                                          backgroundColor: Colors.transparent,
                                          context: context,
                                          builder: (context) =>
                                              BottomSheetRecommendation(
                                            recommendation: params[0]
                                                .recommendation["description"],
                                            description: "Description",
                                          ),
                                        );
                                      },
                                      child: const Text(
                                        "recommendation",
                                        style: TextStyle(
                                          decoration: TextDecoration.underline,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Center(
                                  child: SfRadialGauge(
                                    axes: <RadialAxis>[
                                      RadialAxis(
                                        startAngle: 180,
                                        endAngle: 900,
                                        minimum: 0,
                                        maximum: 100,
                                        showLabels: false,
                                        showTicks: false,
                                        axisLineStyle: const AxisLineStyle(
                                          thickness: 0.2,
                                          // cornerStyle: CornerStyle.bothCurve,
                                          color: Color(0xFFE2FBD7),
                                          thicknessUnit: GaugeSizeUnit.factor,
                                        ),
                                        pointers: <GaugePointer>[
                                          RangePointer(
                                            value: params[0].gasLevel,
                                            width: 0.1,
                                            sizeUnit: GaugeSizeUnit.factor,
                                            color: params[0].gasLevel >= 200
                                                ? Colors.red
                                                : const Color(0xFF34B53A),
                                            cornerStyle: CornerStyle.bothCurve,
                                          ),
                                        ],
                                        annotations: [
                                          GaugeAnnotation(
                                            positionFactor: 0.1,
                                            angle: 90,
                                            widget: Text(
                                              '${params[0].gasLevel.toStringAsFixed(2)} ppm',
                                              style: const TextStyle(
                                                fontSize: 30.0,
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Row(
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          height: 40.0,
                                          width: 40.0,
                                          decoration: BoxDecoration(
                                            color: const Color(0xFF34B53A),
                                            borderRadius:
                                                BorderRadius.circular(50.0),
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 10.0,
                                        ),
                                        const Text("Normal"),
                                      ],
                                    ),
                                    const Spacer(),
                                    Row(
                                      children: [
                                        Container(
                                            height: 40.0,
                                            width: 40.0,
                                            decoration: BoxDecoration(
                                              color: Colors.red,
                                              borderRadius:
                                                  BorderRadius.circular(50.0),
                                            )),
                                        const SizedBox(
                                          width: 10.0,
                                        ),
                                        const Text("Fuite"),
                                      ],
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
