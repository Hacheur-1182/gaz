import 'package:flutter/material.dart';

import '../../helpers/helpers.dart';

class MapIcons extends StatelessWidget {
  const MapIcons({Key? key, required this.tapZoomIcon, required this.btnIcon}) : super(key: key);

  final Function() tapZoomIcon;
  final IconData btnIcon;
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.greenAccent.shade100,
      borderRadius: BorderRadius.circular(15.0),
      child: InkWell(
        splashColor: primaryColor,
        onTap: tapZoomIcon,
        child: SizedBox(
          width: 50,
          height: 50,
          child: Icon(btnIcon),
        )
      ),
    );
  }
}
