class TrackGazModel {
  final double gasLevel;
  final Map<String, dynamic> recommendation;

  TrackGazModel({required this.gasLevel, required this.recommendation});
}
