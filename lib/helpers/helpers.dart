import 'package:flutter/material.dart';

const Color primaryColor = Color(0xFF04BF8A);
const Color authenticationColor = Color(0xFF0099FF);
const Color secondaryColor = Color(0xFFF2F2F2);
const Color cardColor = Color(0xFFD9D9D9);
