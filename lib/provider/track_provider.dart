import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../helpers/endpoint.dart';
import '../models/track_model.dart';
import '../services/get_user.dart';

class TrackGazProvider with ChangeNotifier {
  List<TrackGazModel> _parameters = [];

  UnmodifiableListView<TrackGazModel> get parameters {
    return UnmodifiableListView(_parameters);
  }

  Future<void> getParameters() async {
    try {
      var url = Uri.parse("$endPoint/track");
      var getTokenIn = await getUser();
      final response = await http.get(url, headers: {
        "ContentType": "application/json",
        "Authorization": "Bearer ${getTokenIn.first}"
      });

      final params = await json.decode(response.body) as Map<String, dynamic>;

      print(params);

      if (params["data"] == null) {
        return;
      }
      List<TrackGazModel> extractedParams = [];
      extractedParams.add(
        TrackGazModel(
            gasLevel: params["data"]["gas_level"],
            recommendation: params["data"]["recommendation"]),
      );
      _parameters = extractedParams;
    } catch (error) {
      rethrow;
    }
  }
}
